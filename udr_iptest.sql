use udr
if exists (select * from sysobjects where name = 'OpsIpTest')
	drop table OpsIpTest
go

USE [udr]
GO
/****** Object:  Table [dbo].[OpsIpTest]    Script Date: 12/10/2010 13:55:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OpsIpTest](
	[Action] [nchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Status] [nchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]

GO
INSERT INTO [udr].[dbo].[OpsIpTest]
           ([Action]
           ,[Status])
     VALUES
           ('TEST', 'Inserted')
go
select * from OpsIpTest
GO
UPDATE [udr].[dbo].[OpsIpTest]
           SET [Status] = 'Updated'
go
select * from OpsIpTest
GO



